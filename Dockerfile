FROM python:3.6-slim-buster

WORKDIR /usr/src/app
COPY . /usr/src/app
RUN pip3 install -U pip gunicorn
RUN pip3 install -r requirements.txt

EXPOSE 8008

ENV PYTHONUNBUFFERED=1
ENV PYTHONPATH='/usr/src/app'

CMD ["gunicorn", "-b","0.0.0.0:8008", "-w", "4","--log-level", "debug", "--log-file", "-", "--error-logfile", "-","app.main:app"]