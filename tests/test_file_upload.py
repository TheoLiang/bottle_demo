#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
@Creation: 9/10/20 7:58 PM
@Author: liang
@File: test_file_upload.py
"""
import os

import dataset
import webtest

from app import settings


def test_greeting_ok(api):
    response = api.get('/')
    assert response.status_int == 200


def test_file_upload_with_invalid_number_fail(api: webtest.TestApp):
    file_path = os.path.sep.join([os.path.dirname(__file__), 'invalid_number_dados.txt'])
    response = api.post('/upload', upload_files=[('data', 'dados.txt', open(file_path, 'rb').read())],
                        expect_errors=True)
    assert response.status_int == 500


def test_file_upload_ok(api: webtest.TestApp):
    file_path = os.path.sep.join([os.path.dirname(__file__), 'dados.txt'])
    response = api.post('/upload', upload_files=[('data', 'dados.txt', open(file_path, 'rb').read())])
    assert response.status_int == 200


def test_upload_data_display_ok(api: webtest.TestApp):
    file_path = os.path.sep.join([os.path.dirname(__file__), 'dados.txt'])
    response = api.post('/upload', upload_files=[('data', 'dados.txt', open(file_path, 'rb').read())])
    assert response.status_int == 200

    db = dataset.connect(settings.SQLITE_URL)
    assert db[settings.DATA_TABLE_NAME].count() == 4

    response = api.get('/upload')
    assert response.status_int == 200
    assert len(response.json.get('objects')) == 4
