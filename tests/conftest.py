#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
@Creation: 9/10/20 7:50 PM
@Author: liang
@File: conftest.py
"""
import dataset
import pytest
import webtest

from app import settings
from app.main import app


@pytest.fixture(scope='function')
def api():
    client = webtest.TestApp(app)
    yield client

    db = dataset.connect(settings.SQLITE_URL)
    db[settings.DATA_TABLE_NAME].drop()
