#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
@Creation: 9/10/20 7:51 PM
@Author: liang
@File: settings.py
"""
import os

SQLITE_URL = os.environ.get('SQLITE_URL', 'sqlite:///../eurecicle.db')
DATA_TABLE_NAME = os.environ.get('DATA_TABLE_NAME', 'data')
