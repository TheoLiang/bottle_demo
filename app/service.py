#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
@Creation: 9/10/20 2:02 PM
@Author: liang
@File: service.py
"""
import itertools
from typing import Tuple, Dict

import dataset
from bottle import FileUpload

from app import settings

db = dataset.connect(settings.SQLITE_URL)


class ApiService:

    @classmethod
    def upload_data(cls, datafile: FileUpload) -> Tuple[int, Dict]:
        """
        parse the upload file
        """
        title = list(itertools.islice(datafile.file, 0, 1))[0].decode().strip('\n').split('\t')
        data = (line.decode().strip('\n').split('\t') for line in itertools.islice(datafile.file, 0, None))

        db.begin()
        try:
            for row in data:
                raw_data = dict(zip(title, row))
                cls._normalize_upload_data_row(raw_data)
                db[settings.DATA_TABLE_NAME].insert(raw_data)
            db.commit()
        except Exception as e:
            db.rollback()
            return 500, {'msg': str(e)}

        return 200, {'msg': 'Success'}

    @staticmethod
    def _normalize_upload_data_row(raw_data: Dict) -> Dict:
        raw_data['Preço Unitário'] = float(raw_data['Preço Unitário'])
        raw_data['Quantidade'] = int(raw_data['Quantidade'])

    @staticmethod
    def get_upload_data() -> Tuple[int, Dict]:
        total_value = 0
        objects = []
        for row in db[settings.DATA_TABLE_NAME].all():
            objects.append(row)
            total_value += row['Preço Unitário'] * row['Quantidade']

        return 200, {'objects': objects, 'total_value': total_value}
