#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
@Creation: 9/10/20 2:02 PM
@Author: liang
@File: main.py
"""
from bottle import Bottle, request, response

from app.service import ApiService

app = Bottle()


@app.post('/upload')
def upload_data():
    data_file = request.files.get('data')
    status, msg = ApiService.upload_data(data_file)
    response.status = status
    return msg


@app.get('/upload')
def get_upload_data():
    status, msg = ApiService.get_upload_data()
    response.status = status
    return msg


@app.get('/')
def greeting():
    return {'msg': 'Hello, Eureciclo app!'}


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8008, reloader=True)  # pragma: no cover
