# eureciclo_app test case

## Install
1. Requirements
    - Python3.6
    - Docker
    - git
    
2. Clone the repository to you local system

    `git clone https://gitlab.com/liang.guisheng/eureciclo_app.git  `
    
3. Build docker image
    
    ````shell
    cd eureciclo_app
    docker build -t eureciclo_app . 
    ````
   
4. Run docker container

    `docker run -p 8008:8008 eureciclo_app`


5. Check the greeting endpoint in browser

    `http://localhost:8008/`
    
    you will see this:
    ```json
    {
       "msg": "Hello, Eureciclo app!"
    }
    ```
    

## Api spec doc

### 1. Upload file to system
- **POST** *http://localhost:8008/upload*
- **Content-type**: multipart/form-data
- **Parameters**:
    - data: (file field name) choose the data file which to upload
- **Responses**:
    - 200: Data upload success
    - 500: Data upload with invalid data
        
        
### 2. Get upload data list
- **GET** *http://localhost:8008/upload*
- **Content-type**: application/json
- **Responses**:
    - 200: Data upload success
    ```json
    {
      "objects": [
        {
          "id": 1,
          "Comprador": "João Silva",
          "Descrição": "R$10 off R$20 of food",
          "Preço Unitário": 10.0,
          "Quantidade": 2,
          "Endereço": "987 Fake St",
          "Fornecedor": "Bob's Pizza"
        },
        {
          "id": 2,
          "Comprador": "Amy Pond",
          "Descrição": "R$30 of awesome for R$10",
          "Preço Unitário": 10.0,
          "Quantidade": 5,
          "Endereço": "456 Unreal Rd",
          "Fornecedor": "Tom's Awesome Shop"
        },
        {
          "id": 3,
          "Comprador": "Marty McFly",
          "Descrição": "R$20 Sneakers for R$5",
          "Preço Unitário": 5.0,
          "Quantidade": 1,
          "Endereço": "123 Fake St",
          "Fornecedor": "Sneaker Store Emporium"
        },
        {
          "id": 4,
          "Comprador": "Snake Plissken",
          "Descrição": "R$20 Sneakers for R$5",
          "Preço Unitário": 5.0,
          "Quantidade": 4,
          "Endereço": "123 Fake St",
          "Fornecedor": "Sneaker Store Emporium"
        }
      ],
      "total_value": 95.0
    }
    ```
